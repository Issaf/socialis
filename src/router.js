import Vue from 'vue'
import Router from 'vue-router'
import { auth } from 'firebase'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      meta: {
        requireGuest: true
      },
      path: '/',
      name: 'Home',
      component: () => import(/* webpackChunkName: "home" */ './views/Home.vue')
    },
    {
      meta: {
        requireGuest: true
      },
      path: '/signup',
      alias: ['/login'],
      name: 'Login',
      component: () => import(/* webpackChunkName: "login" */ './views/Login.vue')
    },
    {
      meta: {
        requiresAuth: true
      },
      path: '/contacts',
      name: 'Contacts',
      component: () => import(/* webpackChunkName: "contacts" */ './views/Contacts.vue')
    },
    {
      meta: {
        requiresAuth: true
      },
      path: '/occasions',
      name: 'Occasions',
      component: () => import(/* webpackChunkName: "checklists" */ './views/Occasions.vue')
    },
    {
      meta: {
        requiresAuth: true
      },
      path: '/checklists',
      name: 'Checklists',
      component: () => import(/* webpackChunkName: "checklists" */ './views/Checklists.vue')
    },
    {
      path: '/how-it-works',
      name: 'HowItWorks',
      component: () => import(/* webpackChunkName: "howitworks" */ './views/HowItWorks.vue')
    },
    {
      path: '/privacy-policy',
      name: 'PrivacyPolicy',
      component: () => import(/* webpackChunkName: "privacypolicy" */ './views/PrivacyPolicy.vue')
    },
  ]
})

router.beforeEach((to, from, next) => {
  // Check for requireAuth guard AND not logged in user
  if(to.matched.some(record => record.meta.requiresAuth) && !auth().currentUser) next({path: '/login'})
  // Check for requireGuest guard AND logged in user
  else if(to.matched.some(record => record.meta.requireGuest) && auth().currentUser) next({name: 'Checklists'})
  else next()
})

export default router