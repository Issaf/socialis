import moment from "moment/src/moment"
import axios from "axios"
import { firestore } from "firebase"

export {
  formatDate,
  twoDigitNumber,
  duration,
  sortContactsByName,  

  fetchOccasions,
  updateOccasion,

  fetchContacts,
  deleteContact,
  addContact,
  updateContact,

  fetchChecklists,
  generateChecklists,
  deleteChecklist,
  updateChecklist
}

// =============
// G E N E R A L
// =============

function formatDate(strDate) {
  // day-month-year
  return moment(strDate, "DD-MM-YYYY").format("dddd MMMM Do, YYYY")
}

function sortContactsByName(contactA,contactB) {
  let t1 = contactA.name
  let t2 = contactB.name
  
  if (t1 === t2) return 0
  else if (t1 < t2) return -1
  else return 1
}

function sortOcassionsByDate(a,b) {
  let t1 = moment(a.date, "DD-MM-YYYY")
  let t2 = moment(b.date, "DD-MM-YYYY")
  
  if (t1.isSame(t2)) return 0
  else if (t1.isBefore(t2)) return -1
  else return 1
}

function twoDigitNumber(nbr) {
  nbr = parseInt(nbr)
  return nbr < 10? "0"+nbr : nbr.toString() 
}

function duration(fromDate, toDate, unit="days") {
  let x = fromDate === "today"? moment() : moment(fromDate, "DD-MM-YYYY")
  let y = moment(toDate, "DD-MM-YYYY")
  return Math.trunc(moment.duration(x.diff(y)).as(unit))
}


// =================
// O C C A S I O N S
// =================

function fetchOccasions() {
  return firestore().collection("occasions").get()
  .then(querySnapshot => {
    let unOrderedList = querySnapshot.docs.map(doc => { return {id: doc.id, ...doc.data()} })
    return unOrderedList.sort(sortOcassionsByDate)
  })
}

function updateOccasionValue(occas) {
  let daystoNow = moment.duration(moment().diff(moment(occas.date, "DD-MM-YYYY"))).asDays()
  if ( daystoNow > 7 ) {
    if (["tJuaoeRoSG4wy3wq4tNg", "hqca7zcfBKsGscljJjBh"].includes(occas.id)) { // Eids IDs
      let newHidjri = occas.hidjri.split("-")
      newHidjri[newHidjri.length - 1] = parseInt(newHidjri[newHidjri.length - 1]) + 1
      newHidjri = newHidjri.join("-")

      return axios.get("http://api.aladhan.com/v1/hToG?date=" + newHidjri).then(res => {
        return {
          date: res.data.data.gregorian.date,
          hidjri: res.data.data.hijri.date
        }  
      })
      .catch(() => {
        console.log("Error wit hidjri api")
        return
      })
    } else {
      let dateList = occas.date.split("-")
      dateList[dateList.length - 1] = parseInt(dateList[dateList.length - 1]) + 1
      return Promise.resolve({ date: dateList.join("-") })
    }
  } else {
    return Promise.resolve()
  }

}

function updateOccasion(occas) {
  let id = occas.id
  return updateOccasionValue(occas).then(newVals => {
    if (newVals) return firestore().collection("occasions").doc(id).update(newVals)
    else return Promise.resolve("up-to-date")
  }) 
}


// ===============
// C O N T A C T S
// ===============

function fetchContacts(email) {
  return firestore().collection("contacts").where("owner", "==", email)
  .get()
  .then(querySnapshot => {
    return querySnapshot.docs.length? querySnapshot.docs.map(doc => { return {id: doc.id, ...doc.data()} }) : []
  })
}

function deleteContact(contactId) {
  return firestore().collection("contacts").doc(contactId).delete()
}

function addContact(contact) {
  return firestore().collection("contacts").add(contact)
}

function updateContact(contactId, updates) {
  return firestore().collection("contacts").doc(contactId).update(updates)
}


// ===================
// C H E C K L I S T S
// ===================

function fetchChecklists(email) {
  return firestore().collection("checklists").where("owner", "==", email)
  .get()
  .then(querySnapshot => {
    return querySnapshot.docs.length? querySnapshot.docs.map(doc => { return {id: doc.id, ...doc.data()} }) : []
  })
}

function deleteChecklist(listId) {
  return firestore().collection("checklists").doc(listId).delete()
}

function addChecklist(list) {
  return firestore().collection("checklists").add(list)
}

function updateChecklist(listId, updates) {
  return firestore().collection("checklists").doc(listId).update(updates)
}

function generateChecklists(contacts, occasions, owner) {
  let contactsByOccasion = {}
  let todayFull = moment().format("DD-MM-YYYY")
  let todayDM = moment().format("DD-MM")
  
  contacts.forEach(contact => {
    contact.occasions.forEach(occasion => {
      let dateFull, dateDM, save
      if (occasion.id === "birthday") {
        dateDM = occasion.date.split("-").splice(0,2).join("-")
        save = dateDM === todayDM? true : false
      } else {
        dateFull = occasions.find(x => x.id === occasion.id).date
        save = dateFull === todayFull? true : false
      }
      if (save) {
        if (contactsByOccasion[occasion.id]) contactsByOccasion[occasion.id].push(contact)
        else contactsByOccasion[occasion.id] = [contact]        
      }

    })
  })
  return Object.keys(contactsByOccasion).map(key => {
    let checklist = {
      owner: owner,
      date: todayFull,
      contacts: contactsByOccasion[key].map(x => {
        return { contacted: false, details: x } 
      })
    }

    if (key === "birthday" && contactsByOccasion[key].length > 1) {
        checklist.name = todayFull + " Birthdays"
        checklist.type = "birthday"
    }
    else if (key === "birthday" && contactsByOccasion[key].length === 1) {
      checklist.name = contactsByOccasion[key][0].name + "'s birthday"
      checklist.type = "birthday"
    }
    else {
      checklist.name = occasions.find(x => x.id === key).name + " " + todayFull.split("-")[2]
      checklist.type = "special"
    }
    return addChecklist(checklist)
  })
}