import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        purple: "#764ba2",
        red: "#ED5C52",
        salmon: "#F3A8A2",
        darksalmon: "#F1928D",
        white: "#FCF4F2",
        realwhite: "#fff",
        teal: "#45A8C2",
        beige: "#FAD7AC",
        darkbeige: "#F6B682",
        blue: "#667eea",
        blackish: "#43464b"
      }
    },
  },
  icons: {
    iconfont: 'mdi',
  },
});
