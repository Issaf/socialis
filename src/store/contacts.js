import { fetchContacts, deleteContact, addContact, updateContact, sortContactsByName } from "../services"

export default {
  namespaced: true,
  state: {
    contacts: null,
    loading: false
  },
  mutations: {
    setContacts (state, list) { 
      if (list && list.length) list = list.sort(sortContactsByName)
      state.contacts = list
     },
    setLoading (state, val) { 
      state.loading = val
     }
  },
  actions: {
    fetchAll({commit, rootState}) {
      let email = rootState.user.userDetails.email
      commit("setLoading", true)
      return fetchContacts(email).then(contacts => {
        commit("setContacts", contacts)
        commit("setLoading", false)
        return contacts
      })
    },

    deleteContact({commit,dispatch}, contact) {
      commit("setLoading", true)
      deleteContact(contact.id).then(() => {
        dispatch("fetchAll")
      })
    },

    addContact({rootState, dispatch}, data) {
      data["owner"] = rootState.user.userDetails.email
      return addContact(data).then(() => {
        dispatch("fetchAll")
        return
      })
    },

    updateContact({dispatch}, data) {
      return updateContact(data.id, data.updates).then(() => {
        dispatch("fetchAll")
        return
      })
    }
  }
}