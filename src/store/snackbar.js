export default {
  namespaced: true,
  state: {
    show: false,
    params: {
      status: null,
      msg: ""      
    }
  },
  mutations: {
    setParams (state, obj) { 
      state.params = obj
     },
    setShow (state, val) { 
      state.show = val
     }
  },
  actions: {
    popOut({commit}, obj) {
      commit("setParams", obj)
      commit("setShow", true)
      setTimeout(() => {
        commit("setShow", false)
      }, 5000)
    }
  }
}