export default {
  namespaced: true,
  state: {
    userDetails: null
  },
  getters: {
    isLoggedIn: state => {
      return !!state.userDetails
    }
  },
  mutations: {
    setUser (state, newUser) { 
      state.userDetails = newUser
     }
  },
  // actions: {

  // },
}