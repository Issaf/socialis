import Vue from 'vue'
import Vuex from 'vuex'

import user from './user'
import occasions from './occasions'
import contacts from './contacts'
import checklists from './checklists'
import snackbar from './snackbar'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user,
    occasions,
    contacts,
    checklists,
    snackbar
  }
})
