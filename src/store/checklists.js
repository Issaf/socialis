import { 
  fetchChecklists, 
  deleteChecklist, 
  generateChecklists, 
  updateChecklist, 
  duration
} from "../services"
import moment from "moment/src/moment"

export default {
  namespaced: true,
  state: {
    checklists: null,
    loading: false
  },
  mutations: {
    setChecklists (state, list) {
      state.checklists = list
     },
    setLoading (state, val) { 
      state.loading = val
     }
  },
  actions: {
    fetchAll({commit, rootState, dispatch}) {
      let email = rootState.user.userDetails.email
      commit("setLoading", true)
      fetchChecklists(email).then(lists => {
        // Step 1: delete expired checklists
        let promises = lists.map(list => {
          if (duration("today", list.date) > 7) return deleteChecklist(list.id)
          else return new Promise(resolve => resolve())
        })
        Promise.all(promises).then(() => {
          fetchChecklists(email).then(updatedLists => {
            // Step 2: generate new checklists if needed
            if (updatedLists.map(x => x.date).includes(moment().format("DD-MM-YYYY"))) {
              commit("setChecklists", updatedLists)
              commit("setLoading", false)
            } else {
              dispatch("updateChecklists").then(() => {
                fetchChecklists(email).then(lastLists => {
                  commit("setChecklists", lastLists)
                  commit("setLoading", false)
                })
              })
            }
          })
        })
      })
    },

    updateChecklists({rootState, dispatch}) {
      let owner = rootState.user.userDetails.email
      let contactsPromise, occasionsPromise
      
      // Get Contacts
      if (rootState.contacts.contacts) {
        contactsPromise = new Promise(resolve => resolve( [...rootState.contacts.contacts] ))
      } else {
        contactsPromise = dispatch("contacts/fetchAll",{}, {root:true})
      }
      
      // Get Occasions
      if (rootState.occasions.occasions) {
        occasionsPromise = new Promise(resolve => resolve( [...rootState.occasions.occasions] ))
      } else {
        occasionsPromise = dispatch("occasions/fetchAll",{}, {root:true})
      }
      
      return Promise.all([contactsPromise, occasionsPromise]).then(() => {
        return generateChecklists(rootState.contacts.contacts, rootState.occasions.occasions, owner)
      })
    },

    updateChecklist({dispatch}, data) {
      return updateChecklist(data.id, data.updates).then(() => {
        dispatch("fetchAll")
        return
      })
    }
  }
}