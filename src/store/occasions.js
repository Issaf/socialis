import { fetchOccasions, updateOccasion } from "../services"

export default {
  namespaced: true,
  state: {
    occasions: null,
    loading: false
  },
  mutations: {
    setOccasions (state, list) { 
      state.occasions = list
     },
    setLoading (state, val) { 
      state.loading = val
     }
  },
  actions: {
    fetchAll({commit}) {
      commit("setLoading", true)
      return fetchOccasions().then(occasList => {
        // Update dates
        let promises = occasList.map(occas => {
          return updateOccasion(occas)
        })
        return Promise.all(promises).then(() => {
          // Fetch up to date dates
          return fetchOccasions().then(list => {
            commit("setOccasions", list)
            commit("setLoading", false)        
          })
        })
      })
    }
  }
}